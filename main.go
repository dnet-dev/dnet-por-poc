package main

import ("syscall"
		"log"
		"unsafe"
		"io"
		"io/ioutil"
		"net/http"
		"time"
		"github.com/tcnksm/go-httpstat"
		"fmt"
		"github.com/JohnVentura/por/speedtest"
		"os"
		"flag"
	)
	

	func version() {
		fmt.Print(speedtest.Version)
	}

	func usage() {
		fmt.Fprint(os.Stderr, "Command line interface for testing internet bandwidth using speedtest.net.\n\n")
		flag.PrintDefaults()
	}

	// NetworkInfo model
	type NetworkInfo struct {
		l string
		h string
		d string
		u string
		t string
	}
func main() {

	var displayData []NetworkInfo

	opts := speedtest.ParseOpts()

		switch {
		case opts.Help:
			usage()
			return
		case opts.Version:
			version()
			return
		}

	for t := range time.NewTicker(60* time.Second).C {
		
		client := speedtest.NewClient(opts)
		
		if opts.List {
			servers, err := client.AllServers()
			if err != nil {
				log.Fatalf("Failed to load server list: %v\n", err)
			}
			fmt.Println(servers)
			return
		}
		
		server := selectServer(opts, client);
	
		downloadSpeed := server.DownloadSpeed()
		uploadSpeed := server.UploadSpeed()
		
	
		kernel32, err := syscall.LoadLibrary("Kernel32.dll")
		if err != nil {
			log.Panic(err)
		}
		defer syscall.FreeLibrary(kernel32)
		GetDiskFreeSpaceEx, err := syscall.GetProcAddress(syscall.Handle(kernel32), "GetDiskFreeSpaceExW")    
		if err != nil {
			log.Panic(err)
		}
	
		lpFreeBytesAvailable := int64(0)
		lpTotalNumberOfBytes := int64(0)
		lpTotalNumberOfFreeBytes := int64(0)
		r, a, b := syscall.Syscall6(uintptr(GetDiskFreeSpaceEx), 4,
			uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr("C:"))),
			uintptr(unsafe.Pointer(&lpFreeBytesAvailable)),
			uintptr(unsafe.Pointer(&lpTotalNumberOfBytes)),
			uintptr(unsafe.Pointer(&lpTotalNumberOfFreeBytes)), 0, 0)
		
			// Create a new HTTP request
			req, err := http.NewRequest("GET", "https://github.com", nil)
			if err != nil {
				log.Fatal(err)
			}
			// Create a httpstat powered context
			var result httpstat.Result
			ctx := httpstat.WithHTTPStat(req.Context(), &result)
			req = req.WithContext(ctx)
			// Send request by default HTTP client
			client2 := http.DefaultClient
			res, err := client2.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			if _, err := io.Copy(ioutil.Discard, res.Body); err != nil {
				log.Fatal(err)
			}
			res.Body.Close()
			fmt.Sprintf("%d ms", r)
			fmt.Sprintf("%d ms", a)
			fmt.Sprintf("%d ms", b)
			lat := fmt.Sprintf("%d", int(result.ServerProcessing/time.Millisecond))
			hd := fmt.Sprintf("%d", lpTotalNumberOfFreeBytes/1024/1024.0)
			dl := fmt.Sprintf("%.2f", float64(downloadSpeed) / (1 << 20))
			ul := fmt.Sprintf("%.2f", float64(uploadSpeed) / (1 << 17))
			tm := fmt.Sprintf(t.Format("2006-01-02 15:04:05"))

			displayData = append(displayData, NetworkInfo{l:lat,h:hd,d:dl,u:ul,t:tm})
			fmt.Printf("%+v\n", displayData)
    }

	

}

func selectServer(opts *speedtest.Opts, client *speedtest.Client) (selected *speedtest.Server) {
	if opts.Server != 0 {
		servers, err := client.AllServers()
		if err != nil {
			log.Fatal(err)
			return nil
		}
		selected = servers.Find(opts.Server)
		if selected == nil {
			log.Fatalf("Server not found: %d\n", opts.Server)
			return nil
		}
		selected.MeasureLatency(speedtest.DefaultLatencyMeasureTimes, speedtest.DefaultErrorLatency)
	} else {
		servers, err := client.ClosestServers()
		if err != nil {
			log.Fatal(err)
			return nil
		}
		selected = servers.MeasureLatencies(
			speedtest.DefaultLatencyMeasureTimes,
			speedtest.DefaultErrorLatency).First()
	}

	return selected
}